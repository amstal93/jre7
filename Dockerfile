ARG CI_REGISTRY
FROM debian:jessie-backports

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install openjdk-7-jre
